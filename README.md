# Liya Build Framework

### DEBRAND ###
1. Change the logo in /usr/share/pixmaps
<br>
2. Change GRUB And SYSLINUX Themes
<br>
3. Change GRUB in /etc/default/grub
<br>
4. Change Name in /etc/lsb-release2 and /usr/lib/os-release_2
<br>
5. Change name in calamares (/etc/calamares/;branding/default;modules/shellprocess-after.conf)
<br>
6. Change name in motd file (/etc/motd)
<br>
7. Change liya-update to your own (/usr/local/bin/liya-update)